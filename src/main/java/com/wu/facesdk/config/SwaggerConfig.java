package com.wu.facesdk.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;

/**
 * @ClassName: SwaggerConfig
 * @description: TODO
 * @author: xxxx
 * @create: 2021-08-17 09:58
 * @Version 1.0
 **/
@Configuration
@EnableSwagger2
public class SwaggerConfig {

    //配置Swagger的Docket实例
    //enable 是否启用swagger，如果为false，则不能在浏览器中访问
    @Bean
    public Docket docket(){

        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .enable(true);
    }

    //配置Swagger信息apiInfo()
    private ApiInfo apiInfo(){

        Contact contact = new Contact("吴尤", "https://mail.qq.com/", "2213093478@qq.com");

        return new ApiInfo("吴尤的Swagger接口测试",
                "别出BUG！",
                "1.0",
                "https://mail.qq.com/",
                contact,
                "Apache 2.0",
                "http://www.apache.org/licenses/LICENSE-2.0",
                new ArrayList());
    }
}