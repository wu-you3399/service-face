package com.wu.facesdk.service.impl;

import com.wu.facesdk.service.FileService;
import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import com.wu.facesdk.util.ConstantOssPropertiesUtils;
import org.joda.time.DateTime;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;

/**
 * @ClassName: FileServiceImpl
 * @description: TODO
 * @author: xxxx
 * @create: 2021-10-11 22:08
 * @Version 1.0
 **/
@Service
public class FileServiceImpl implements FileService {
    @Override
    public String upload(MultipartFile file) {
        // yourEndpoint填写Bucket所在地域对应的Endpoint。以华东1（杭州）为例，Endpoint填写为https://oss-cn-hangzhou.aliyuncs.com。
        String endpoint = ConstantOssPropertiesUtils.ENDPOINT;
        // 通过封装配置类读取属性
        String accessKeyId = ConstantOssPropertiesUtils.ACCESS_KEY_ID;
        String accessKeySecret = ConstantOssPropertiesUtils.SECRECT;
        String bucketName = ConstantOssPropertiesUtils.BUCKET;

        // 创建OSSClient实例。
        OSS ossClient = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);
        // 上传文件流
        try {
            InputStream inputStream = file.getInputStream();
            String fileName = file.getOriginalFilename();
            // 生成随机唯一值，使用UUID,添加到文件名称中
            String uuid = UUID.randomUUID().toString().replaceAll("-","");

            // 按照当前日期，创建文件夹，上传到创建文件夹里面
            String timeUrl = new DateTime().toString("yyyy/MM/dd");

            // 设置包含文件夹的绝对路径
            String fileUrl = "faceSdk/"+timeUrl+"/"+uuid+fileName;

            // 调用方法实现上传
            ossClient.putObject(bucketName,fileUrl,inputStream);
            // 关闭OSSClient。
            ossClient.shutdown();

            // 返回上传之后的文件路径
            String url = ConstantOssPropertiesUtils.FILE_PATH+fileUrl;
            return url;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
