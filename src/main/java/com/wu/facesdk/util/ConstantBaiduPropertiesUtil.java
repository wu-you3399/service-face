package com.wu.facesdk.util;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.Serializable;
import java.util.Properties;

/**
 * @ClassName: ConstantAnalysisModelPropertiesUtil
 * @description: TODO
 * @author: xxxx
 * @create: 2021-10-20 21:02
 * @Version 1.0
 **/
@Component
public class ConstantBaiduPropertiesUtil implements InitializingBean {
    @Value("${BAIDU.FACEAPI.APPKEY}")
    private String appKey;

    @Value("${BAIDU.FACEAPI.SECREKEY}")
    private String secreKey;

    @Value("${BAIDU.FACEAPI.FACEDETECTURL}")
    private String faceDetectUrl;

    @Value("${BAIDU.FACEAPI.ACCESSTOKENURL}")
    private String accessTokenUrl;

    public static String BAIDU_FACEAPI_APPKEY;//百度人脸识别APIKEY
    public static String BAIDU_FACEAPI_SECREKEY;   //百度人脸识别APISecretKey
    public static String BAIDU_FACEAPI_FACEDETECTURL;//百度人脸识别API请求URL
    public static String BAIDU_FACEAPI_ACCESSTOKENURL;  //百度人脸识别AccessToken请求Url

    @Override
    public void afterPropertiesSet(){
        BAIDU_FACEAPI_APPKEY = appKey;
        BAIDU_FACEAPI_SECREKEY = secreKey;
        BAIDU_FACEAPI_FACEDETECTURL = faceDetectUrl;
        BAIDU_FACEAPI_ACCESSTOKENURL = accessTokenUrl;
    }
}
