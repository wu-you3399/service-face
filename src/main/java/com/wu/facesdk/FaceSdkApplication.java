package com.wu.facesdk;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FaceSdkApplication {

    public static void main(String[] args) {
        SpringApplication.run(FaceSdkApplication.class, args);
    }

}
