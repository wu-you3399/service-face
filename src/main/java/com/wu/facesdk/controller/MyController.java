package com.wu.facesdk.controller;

import com.wu.facesdk.result.Result;
import com.wu.facesdk.service.FileService;
import com.wu.facesdk.util.FaceDetectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.util.Map;

/**
 * @ClassName: FileApiController
 * @description: TODO
 * @author: xxxx
 * @create: 2021-10-11 22:04
 * @Version 1.0
 **/
@Controller
public class MyController {

    @Autowired
    private FileService fileService;

    // 上传文件到阿里云oss
    @PostMapping("fileUpload")
    @ResponseBody
    public Result fileUpload(MultipartFile file){
        // 获取上传文件地址
        String url = fileService.upload(file);
        Map<String, Object> map = FaceDetectUtils.ScoreResult("URL", url);
        if (ObjectUtils.isEmpty(map)){
            return Result.fail();
        }
        return Result.ok(map);
    }

    @PostMapping("imageUpload")
    @ResponseBody
    public Result imageUpload(String img,
                              Model model){
        Map<String, Object> map = FaceDetectUtils.ScoreResult("BASE64", img);
        if (ObjectUtils.isEmpty(map) || map.get("errMessage") != null){
            return Result.fail();
        }
        model.addAttribute("msg",1);
        return Result.ok(map);
    }

}
