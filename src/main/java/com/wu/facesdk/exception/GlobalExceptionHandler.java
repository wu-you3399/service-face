package com.wu.facesdk.exception;

import com.wu.facesdk.result.Result;
import com.wu.facesdk.result.ResultCodeEnum;
import org.apache.tomcat.util.http.fileupload.impl.FileSizeLimitExceededException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

@ControllerAdvice
public class GlobalExceptionHandler {
//    @ExceptionHandler(Exception.class)
//    @ResponseBody
//    public Result error(Exception e){
//        e.printStackTrace();
//        return Result.fail();
//    }

    /**
     * 自定义异常处理方法
     * @param e
     * @return
     */
    @ExceptionHandler(FileSizeLimitExceededException.class)
    @ResponseBody
    public Result error(FileSizeLimitExceededException e){
        return Result.build(ResultCodeEnum.FILE_TOO_BIG);
    }
}